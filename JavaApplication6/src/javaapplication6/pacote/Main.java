/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication6.pacote;

/**
 *
 * @author ice
 */
public class Main {
    
    public static void main(String[] args){
        
        Estoque e1,e2, e3, e4;
        e1=new Estoque();
        e2=new Estoque();
        e3=new Estoque();
        e4=new Estoque();
        
        e1.setNome("Impressora Jato de tinta");
        e1.setQuantidadeAtual(13);
        e1.setQuantidadeMinima(6);
        
        e2.setNome("Monitor LCD 17 polegadas");
        e2.setQuantidadeAtual(11);
        e2.setQuantidadeMinima(13);
        
        e3.setNome("Mouse optico");
        e3.setQuantidadeAtual(6);
        e3.setQuantidadeMinima(2);
        
        e4.setNome("Headset");
        e4.setQuantidadeAtual(15);
        e4.setQuantidadeMinima(5);
        
        e1.info();
        e2.info();
        e3.info();
        e4.info();
        
        System.out.println("============================================================");
        
        e1.baixa(5);
        e2.repor(7);
        e3.baixa(4);
        
        e1.verifica();
        e2.verifica();
        e3.verifica();
        e4.verifica();
        
        e1.info();
        e2.info();
        e3.info();
        e4.info();
        
        
        
    }
    
}
