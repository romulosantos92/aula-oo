/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication6.pacote;

/**
 *
 * @author ice
 */
public class Estoque {
    private String nome;
    private int quantidadeAtual;
    private int quantidadeMinima;
    
    Estoque(){
        nome="nome";
        quantidadeAtual=0;
        quantidadeMinima=0;  
    }
    public void setNome(String str){
        nome=str;
    }
    public String getNome(){
        return nome;
    }
    public void setQuantidadeAtual(int v){
        quantidadeAtual=v;
    }
    public int getQuantidadeAtual(){
        return quantidadeAtual;
    }
    public void setQuantidadeMinima(int v){
        quantidadeMinima=v;
    }
    public int getQuantidadeMinima(){
        return quantidadeMinima;
    }
    
    public void repor(int v){
        setQuantidadeAtual(getQuantidadeAtual()+v);
    }
    public void baixa(int v){
        if((getQuantidadeAtual()-v)<0){
            System.out.println("Impossivel dar baixa - Baixa excede estoque");
        }
        else
            setQuantidadeAtual(getQuantidadeAtual()-v);
    }
    public void info(){
        System.out.println("==============================");
        System.out.println("Nome: " + getNome());
        System.out.println("Quantidade atual: "+ getQuantidadeAtual());
        System.out.println("Quantidade minima: " + getQuantidadeMinima());
        System.out.println("==============================");
    }
    public void verifica(){
        if(getQuantidadeAtual()<=getQuantidadeMinima())
            System.out.println("O estoque de " + getNome() + " Precisa ser reposto");
    }
}
